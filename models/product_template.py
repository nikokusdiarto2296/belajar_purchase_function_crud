# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
import string, random

class ProductTemplate(models.Model):
    _inherit = "product.template"

    @api.model
    def create(self, vals):
        vals['default_code'] = str(''.join(random.choices(string.ascii_uppercase + string.digits, k = 10)))
        res = super(ProductTemplate, self).create(vals)
        return res

    def write(self, vals):
        if len(vals['default_code']) != 10:
            raise ValidationError(_('The internal reference format is wrong make sure it has 10 digit'))
        res = super(ProductTemplate, self).write(vals)
        return res

