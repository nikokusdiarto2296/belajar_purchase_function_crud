# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Belajar Purchase Function CRUD',
    'version': '1.2',
    'category': 'Operations/Purchase',
    'summary': 'Module Untuk Belajar Purchase Function CRUD',
    'description': "Module Ini Sebagai Bentuk Pembelajaran Modifikasi Fungsi CRUD Bawaan.",
    'website': 'https://www.odoo.com/page/purchase',
    'depends': ['purchase'],
    'data': [],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'LGPL-3',
}
